const video = document.getElementById("video");
const videoContainer = document.getElementById("video-container");

const options =
    Promise.all([
        faceapi.nets.ssdMobilenetv1.loadFromUri("/models"),
        faceapi.nets.faceRecognitionNet.loadFromUri("/models"),
        faceapi.nets.faceLandmark68Net.loadFromUri("/models"),
        faceapi.nets.faceLandmark68TinyNet.loadFromUri("/models"),
        faceapi.nets.tinyFaceDetector.loadFromUri("/models"),
        faceapi.nets.ageGenderNet.loadFromUri("/models"),
        faceapi.nets.mtcnn.loadFromUri("/models"),

        // faceapi.nets.ssdMobilenetv1.loadFromUri("/absenWajah/models"),
        // faceapi.nets.faceRecognitionNet.loadFromUri("/absenWajah/models"),
        // faceapi.nets.faceLandmark68Net.loadFromUri("/absenWajah/models"),
        // faceapi.nets.faceLandmark68TinyNet.loadFromUri("/absenWajah/models"),
        // faceapi.nets.tinyFaceDetector.loadFromUri("/absenWajah/models"),
        // faceapi.nets.ageGenderNet.loadFromUri("/absenWajah/models"),
        // faceapi.nets.mtcnn.loadFromUri("/absenWajah/models"),

    ]).then(startWebcam);

function startWebcam() {
    var constraints = {
        audio: false, video: true
    };
    navigator.mediaDevices
        .getUserMedia(constraints)
        .then((stream) => {
            video.srcObject = stream;
        })
        .catch(function (err) {
            alert('Kamera Anda tidak aktif');
        });
}


function getLabeledFaceDescriptions() {
    return Promise.all(
        labels.map(async (label) => {
            const descriptions = [];
            for (let i = 1; i <= 5; i++) {
                const img = await faceapi.fetchImage(`./labels/${label}/${i}.jpg`);
                const detections = await faceapi
                    .detectSingleFace(img)
                    .withFaceLandmarks()
                    .withAgeAndGender()
                    .withFaceDescriptor();
                if (detections?.descriptor) {
                    descriptions.push(detections?.descriptor);
                }
            }
            return new faceapi.LabeledFaceDescriptors(label, descriptions);
        })
    );
}

const mtcnnForwardParams = {
    maxNumScales: 10,
    scaleFactor: 0.709,
    scoreThresholds: [0.6, 0.7, 0.7],
    minFaceSize: 200,
};

video.addEventListener("play", async () => {
    const labeledFaceDescriptors = await getLabeledFaceDescriptions();
    const threshold = 0.6;
    const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, threshold);
    const mtcnnResults = await faceapi.mtcnn(video, mtcnnForwardParams);

    // const canvas = faceapi.createCanvasFromMedia(video);
    // document.body.append(canvas);
    //
    // const displaySize = {width: video.width, height: video.height};
    // faceapi.matchDimensions(canvas, displaySize);

    const canvas = $("#overlay").get(0);
    const dims = faceapi.matchDimensions(canvas, video, true);

    // const options = getFaceDetectorOptions()
    const options = new faceapi.MtcnnOptions(mtcnnForwardParams);

    let absenCek = [];

    setInterval(async () => {
        const detections = await faceapi
            .detectAllFaces(video, options)
            .withFaceLandmarks()
            .withFaceDescriptors();

        const resizedDetections = faceapi.resizeResults(detections, dims);

        canvas.getContext('2d', {willReadFrequently: true}).clearRect(0, 0, canvas.width, canvas.height);

        const results = resizedDetections.map((d) => {
            return faceMatcher.findBestMatch(d.descriptor);
        });

        function dateToString(date) {
            var month = date.getMonth() + 1;
            var day = date.getDate();
            var dateOfString = (("" + day).length < 2 ? "0" : "") + day + "/";
            dateOfString += (("" + month).length < 2 ? "0" : "") + month + "/";
            dateOfString += date.getFullYear();
            return dateOfString;
        }

        var currentdate = new Date();
        var datetime = "";
        datetime += dateToString(currentdate);
        datetime += +currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

        results.forEach((result, i) => {
            const box = resizedDetections[i].detection.box;
            const drawBox = new faceapi.draw.DrawBox(box, {
                // label: result._label +
                //     '\n Umur: ' + Math.round(resizedDetections[i].age) + " Tahun. \n JK: " +
                //     (resizedDetections[i].gender === 'male' ? 'Pria' : 'Wanita') + ''
                label: 'NIM: ' + result._label + '\n' + 'Akurasi: ' + (result._distance.toFixed(2)) * 100,
                boxColor:  result._label !== 'unknown' ? 'rgb(59,174,2)' : 'rgba(248,0,0,0.5)'
            });
            drawBox.draw(canvas);

            // new faceapi.draw.drawFaceLandmarks(canvas, resizedDetections[i]);
            if (result._label !== 'unknown') {
                absenCek.push({nims: result._label});
                if (absenCek.filter(e => e.nims === result._label).length > 200) {
                    if (absen.filter(e => e.nim === result._label).length <= 0) {
                        axios
                            .get("./getMhs.php?nim=" + result._label, {
                                responseType: "json",
                            })
                            .then(function (response) {
                                if (absen.filter(e => e.nim === result._label).length <= 0) {
                                    absen.push({
                                        nim: result._label,
                                        nama: response.data,
                                        dosen: dosen,
                                        kelas: kelas,
                                        tanggal: moment().format('DD-MM-YYYY'),
                                        jam: moment().format('HH:mm:ss'),
                                        pertemuan: pertemuan,
                                    });
                                }
                            });
                    }
                }
            }
        });
    }, 100);

    setInterval(async () => {
        if (absen.length > 0) {
            var newTable = "<table class='table table-striped table-bordered table-success'>" +
                "<thead class='table-light'>" +
                "<tr>" +
                "<th>No</th>" +
                "<th>NIM</th>" +
                "<th>Nama Mahasiswa</th>" +
                "<th>Tanggal</th>" +
                "<th>Jam</th>" +
                "</tr>";
            "</thead>" +
            "<tbody>";
            let no = 1;
            for (j = 0; j < absen.length; j++) {
                newTable += "<tr>" +
                    "<td align='center'>" + no + "</td>" +
                    "<td align='center'>" + absen[j]['nim'] + "</td>" +
                    "<td align='center'>" + absen[j]['nama'] + "</td>" +
                    "<td align='center'>" + absen[j]['tanggal'] + "</td>" +
                    "<td align='center'>" + absen[j]['jam'] + "</td>" +
                    "</tr>";
                no++;
            }
            newTable += "</tbody>" +
                "</table>";

            document.getElementById("dataAbsen").innerHTML = newTable;
        }
    }, 2000);
});
