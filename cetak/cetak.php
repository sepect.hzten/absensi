<?php
require_once '../dompdf/autoload.inc.php';
use Dompdf\Dompdf;

$kelas = $_GET['kelas'];
include '../koneksi.php';

$dt = mysqli_fetch_assoc(mysqli_query($koneksi,"SELECT * FROM tb_kelas inner join tb_dosen on tb_kelas.id_dosen = tb_dosen.id_dosen where tb_kelas.id_kelas = '$kelas'"));
$data = mysqli_query($koneksi, "SELECT A.id_laporan as idLaporan,A.id_kelas as idKelas,C.nama_kelas as namaKelas, A.nim as nimL, D.nama_mahasiswa as namaMahasiswa FROM tb_laporan A 
    INNER JOIN tb_dosen B ON A.id_dosen = B.id_dosen 
    INNER JOIN tb_kelas C ON A.id_kelas = C.id_kelas 
    INNER JOIN tb_mahasiswa D ON A.nim = D.nim where A.id_kelas = '$kelas' GROUP BY namaKelas, nimL");
$datakelas = mysqli_query($koneksi, "SELECT * FROM tb_laporan A
    INNER JOIN tb_kelas C ON A.id_kelas = C.id_kelas where A.id_kelas = '$kelas' GROUP BY A.pertemuan");

$dompdf = new Dompdf();
$html = '<html lang="en">';
$html .= '<head>
<title>Cetak laporan</title>
<style>
        td {
            text-align: center;
        }
    </style>
</head>
<body style="justify-content: center">
<div style="text-align: center;padding: 10px;width: 100%">
    <h2>DAFTAR HADIR MAHASISWA</h2>
</div>
';
$html .= '<div style="justify-content: start;padding: 10px;display: flex;flex: 1">';
$html .= '<table>';
$html .= '<tr>';
$html .= '<td style="font-size: 20px;width: 100px;text-align: left">Semester</td>';
$html .= '<td style="font-size: 20px;text-align: left;width: 700px;">: <b>'.$dt['semester'].'</b></td>';
$html .= '<td style="font-size: 20px;width: 100px;text-align: left">Kelas</td>';
$html .= '<td style="font-size: 20px;text-align: left">: <b>'.$dt['nama_kelas'].'</b></td>';
$html .= '</tr>';
$html .= '<tr>';
$html .= '<td style="font-size: 20px;width: 100px;text-align: left">Mata Kuliah</td>';
$html .= '<td style="font-size: 20px;text-align: left;width: 700px;">: <b>'.$dt['matakuliah'].'</b></td>';
$html .= '<td style="font-size: 20px;width: 100px;text-align: left">SKS</td>';
$html .= '<td style="font-size: 20px;text-align: left">: <b>'.$dt['sks'].'</b></td>';
$html .= '</tr>';
$html .= '<tr>';
$html .= '<td style="font-size: 20px;width: 100px;text-align: left">Hari/Waktu</td>';
$html .= '<td style="font-size: 20px;text-align: left;width: 700px;">: <b>'.$dt['hari'].'</b> / '.$dt['waktu'].'</td>';
$html .= '<td style="font-size: 20px;width: 100px;text-align: left">Ruangan</td>';
$html .= '<td style="font-size: 20px;text-align: left">: <b>'.$dt['ruangan'].'</b></td>';
$html .= '</tr>';
$html .= '<tr>';
$html .= '<td style="font-size: 20px;width: 100px;text-align: left">Dosen</td>';
$html .= '<td style="font-size: 20px;text-align: left;width: 700px;">: <b>'.$dt['nama_dosen'].'</b></td>';
$html .= '</tr>';
$html .= '</table>';
$html .= '</div>';
$html .= '<table style="width: 100%" border="1">
    <thead>
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2" width="10%">NIM</th>
        <th rowspan="2" width="30%">NAMA</th>
        <th colspan="5">Keterangan Kehadiran</th>
    </tr>
    <tr>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
    </tr>
    </thead>
    <tbody>';
    $no = 1;
    foreach ($data as $r) {
        $html .= '<tr style="height: 50px">';
        $html .= '<td style="text-align: center;">'.$no++.'</td>';
        $html .= '<td style="text-align: center;">'.$r['nimL'].'</td>';
        $html .= '<td style="text-align: center;">'.$r['namaMahasiswa'].'</td>';
            for ($i=1;$i<=5;$i++){
                $html .= '<td style="text-align: center;">';
                $cekHadir = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM tb_laporan  where id_kelas = '$kelas' and pertemuan = '$i' and nim = '".$r['nimL']."'"));
                $cekPertemuan = mysqli_num_rows(mysqli_query($koneksi,"SELECT * FROM tb_laporan  where id_kelas = '$kelas' and pertemuan = '$i'"));
                if ($cekHadir > 0 && $cekPertemuan > 0){
                    $html .= '<span style="padding: 5px;background-color: aqua;border-radius: 10px">Hadir</span>';
                }elseif($cekHadir >= 0  && $cekPertemuan > 0){
                    $html .= '<span style="padding: 5px;background-color: #ff004c;border-radius: 10px;color: antiquewhite">Tidak Hadir</span>';
                }elseif($cekHadir >= 0  && $cekPertemuan <= 0){
                    $html .= '<span>Belum ada pertemuan</span>';
                }
                $html .= '</td>';
            }
        $html .= '</tr>';
    }
$html .= '</tbody>
</table>
</body>
</html>';
// Load HTML content
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('LEGAL', 'landscape');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream('laporan_siswa.pdf');