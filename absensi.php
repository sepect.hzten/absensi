<?php
include 'koneksi.php';
$kelas = $_GET['kelas'];
$dosen = $_GET['dosen'];
$pertemuan = $_GET['pertemuan'];
$data = mysqli_query($koneksi, "SELECT * FROM tb_mahasiswa where id_kelas = '$kelas'");
$mhs = [];
foreach ($data as $y) {
    $mhs[] = $y['nim'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Absen Mahasiswa</title>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.30.1/moment.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
            crossorigin="anonymous"></script>
    <script defer src="dist/face-api.min.js"></script>
    <script defer src="js/faceDetectionControls.js"></script>
    <script defer src="js/imageSelectionControls.js"></script>
    <script defer src="script.js"></script>
    <style>
        .center-content {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            flex-wrap: wrap;
        }

        .margin {
            margin: 20px;
        }

        #overlay, .overlay {
            position: absolute;
            top: 0;
            left: 0;
        }

        #facesContainer canvas {
            margin: 10px;
        }
    </style>
</head>
<body>
<div class="center-content mt-5">
    <h4>Silahkan lakukan absen</h4>
    <div style="position: relative" class="margin">
        <video id="video" autoplay muted playsinline></video>
        <canvas id="overlay" />
    </div>

    <div class="row">
        <div class="col-12 text-center">
            <p>Data Mahasiswa berhasil absen</p>
            <div id="dataAbsen" class="text-center">
            </div>
        </div>

        <button type="button" class="btn btn-lg btn-success" onclick="onSimpan()">Simpan Absen Kehadiran Mahasiswa
        </button>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
        integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/1.6.5/axios.min.js"
        integrity="sha512-TjBzDQIDnc6pWyeM1bhMnDxtWH0QpOXMcVooglXrali/Tj7W569/wd4E8EDjk1CwOAOPSJon1VfcEt1BI4xIrA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    // let labels = ['201056','201073','201048'];
    var kelas = <?php echo $kelas; ?>;
    var dosen = <?php echo $dosen; ?>;
    var pertemuan = <?php echo $pertemuan; ?>;
    let absen = [];
    
    function onSimpan() {
        if (absen.length <= 0) {
            Swal.fire({
                title: 'INFO',
                text: 'Data Absen Kehadiran Masih Kosong!',
                icon: 'info',
            });
        } else {
            // console.log(JSON.stringify(absen));
            axios.post("./saveAbsensi.php", absen)
                .then(function (response) {
                    // console.log(response.data);
                    if (response.data === 1){
                        Swal.fire({
                            icon: 'success',
                            title: 'Data Absensi Berhasil Disimpan.',
                            showConfirmButton: true,
                        }).then((result) => {
                            location='./user/?page=laporan';
                        });
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Data Gagal Disimpan.',
                            showConfirmButton: true,
                        }).then((result) => {
                            location='./user/?page=laporan';
                        });
                    }
                });
        }
    }
</script>
</body>
</html>