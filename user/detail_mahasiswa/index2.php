<?php
$id = $_GET['id'];
$dt = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT A.id_kelas as idKelas,
       A.id_dosen as idDosen,
       A.id_dosen as idDosen,
       A.tanggal_absen as tglAbsen,
       A.waktu as waktuH,
       A.pertemuan as pertemuanP,
       A.nim as nimL,
       C.nama_kelas as nmKelas
       FROM tb_laporan A 
    INNER JOIN tb_dosen B ON A.id_dosen = B.id_dosen 
    INNER JOIN tb_kelas C ON A.id_kelas = C.id_kelas 
    INNER JOIN tb_mahasiswa D ON A.nim = D.nim WHERE A.id_laporan = '$id'"));

$data = mysqli_query($koneksi, "SELECT A.id_laporan as idLaporan,
       A.id_kelas as idKelas,
       A.id_dosen as idDosen,
       A.tanggal_absen as tglAbsen,
       A.waktu as waktuH,
       A.pertemuan as pertemuanP,
       A.nim as nimL, 
       D.nama_mahasiswa as namaMahasiswa, 
       D.foto_mahasiswa as fotoMahasiswa 
FROM tb_laporan A 
    INNER JOIN tb_dosen B ON A.id_dosen = B.id_dosen 
    INNER JOIN tb_kelas C ON A.id_kelas = C.id_kelas 
    INNER JOIN tb_mahasiswa D ON A.nim = D.nim WHERE A.nim = '" . $dt['nimL'] . "' and A.id_kelas = '" . $dt['idKelas'] . "' and A.id_dosen = '".$idLogin."' GROUP BY A.waktu ORDER BY A.tanggal_absen,A.waktu ASC");
$jml = mysqli_num_rows($data);
$persentase = ($jml / 5) * 100;
?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="card dz-card" id="accordion-one">
                    <div class="card-header flex-wrap">
                        <div>
                            <h4 class="card-title">Detail Absensi <?= $dt['id_kelas'] ?></h4>
                            <hr>
                            <h3><?= $dt['nama_mahasiswa']; ?></h3>
                            <hr>
                            <div class="ms-auto col-md-12">
                                <h3>Persentase Kehadiran <?= round($persentase) ?>%</h3>
                                <?php
                                if ($persentase >= 75){
                                    echo '
                                        <p class="text-success"> Sudah memenuhi syarat mengikuti final </p>
                                        ';
                                }else{
                                    echo '
                                        <p class="text-danger"> Belum memenuhi syarat mengikuti final </p>
                                        ';
                                }
                                ?>
                            </div>
                            <br>
                            <div class="ms-auto col-md-12">
                                <button type="button" class="btn btn-info" onclick="location.href='?page=laporan'">Kembali</button>
                            </div>
                        </div>
                    </div>
                    <!--tab-content-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Preview" role="tabpanel"
                             aria-labelledby="home-tab">
                            <div class="card-body pt-0">
                                <div class="table-responsive">
                                    <table id="example" class="display table" style="min-width: 845px">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Foto Mahasiswa</th>
                                            <th>NIM</th>
                                            <th>Nama Mahasiswa</th>
                                            <th>Kelas</th>
                                            <th>Tanggal Hadir</th>
                                            <th>Waktu Hadir</th>
                                            <th>Keterangan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($data as $t) {
                                            ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td>
                                                    <img src="uploads/<?= $t['fotoMahasiswa']; ?>" alt="mahasiswa"
                                                         width="200">
                                                </td>
                                                <td><?= $t['nimL']; ?></td>
                                                <td><?= $t['namaMahasiswa']; ?></td>
                                                <td><?= $dt['nmKelas']; ?></td>
                                                <td><?= $t['tglAbsen']; ?></td>
                                                <td><?= $t['waktuH']; ?></td>
                                                <td style="font-weight: bold">Pertemuan <?= $t['pertemuanP']; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/tab-content-->
                </div>
            </div>
        </div>
    </div>
</div>