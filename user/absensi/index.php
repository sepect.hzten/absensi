<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-8">
                <div class="card dz-card" id="accordion-one">
                    <div class="card-header flex-wrap">
                        <div>
                            <h4 class="card-title">Absensi Mahasiswa</h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="" method="post">
                            <input type="hidden" name="dosen" value="<?= $_SESSION['id']; ?>">
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label">Kelas Mahasiswa</label>
                                <div class="col-sm-9">
                                    <select name="kelas" id="kelas" class="form-control" required>
                                        <option value="" disabled selected>Pilih Kelas</option>
                                        <?php
                                        $data = mysqli_query($koneksi, "SELECT * FROM tb_kelas WHERE id_dosen = '$idLogin'");
                                        foreach ($data as $t) {
                                            ?>
                                            <option value="<?= $t['id_kelas'] ?>"><b><?= $t['matakuliah'] ?>
                                                    | <?= $t['nama_kelas'] ?></b></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <label class="col-sm-3 col-form-label">Jenis Absensi</label>
                                <div class="col-sm-9">
                                    <select name="absen" id="absen" class="form-control" required>
                                        <option value="" disabled selected>Pilih Absensi</option>
                                        <option value="ONLINE">ONLINE</option>
                                        <option value="OFFLINE">OFFLINE</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <div class="col-sm-4">
                                    <button type="submit" name="kirim" class="btn btn-primary">Lakukan Absen</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<?php
date_default_timezone_set('Asia/Makassar');

if (isset($_POST['kirim'])) {
    $kelas = $_POST['kelas'];
    $absen = $_POST['absen'];

    $dt = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT MAX(pertemuan) as JmlP FROM tb_laporan where id_kelas = '$kelas'"));
    $pertemuan = $dt['JmlP'] + 1;

    if ($dt['JmlP'] > 5){
        echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Pertemuan hanya maksimal 5 kali',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=absensi';
    });
    </script>";
    }else {

        if ($absen === 'ONLINE') {
            $data = mysqli_query($koneksi, "SELECT * FROM tb_mahasiswa where id_kelas = '$kelas'");
            foreach ($data as $d) {
                $simpan = mysqli_query($koneksi, "INSERT INTO tb_laporan VALUES (NULL,
                               '" . $idLogin . "',
                               '" . $kelas . "',
                               '" . $d['nim'] . "',
                               '" . date('Y-m-d') . "',
                               '" . date('H:i:s') . "',
                               '" . $absen . "',
                               '" . $pertemuan . "'
                               )");
            }
            if ($simpan) {
                echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=laporan';
    });
    </script>";
            } else {
                echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=laporan';
    });
    </script>";
            }
        } else {
            header('Location: ../absensi.php?kelas=' . $kelas . '&&dosen=' . $idLogin . '&&pertemuan=' . $pertemuan);
        }
    }

}