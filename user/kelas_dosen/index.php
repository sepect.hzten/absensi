<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="card dz-card" id="accordion-one">
                    <div class="card-header flex-wrap">
                        <div>
                            <h4 class="card-title">Data Kelas</h4>
                        </div>
                    </div>
                    <!--tab-content-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Preview" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card-body pt-0">
                                <div class="table-responsive">
                                    <table id="tabel" class="display table" style="min-width: 845px">
                                        <thead>
                                        <tr>
                                            <th>No</th>
<!--                                            <th>NIDN</th>-->
<!--                                            <th>Nama Dosen</th>-->
                                            <th>Kelas</th>
                                            <th>Mata Kuliah</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        $data = mysqli_query($koneksi, "SELECT * FROM tb_kelas inner join tb_dosen on tb_kelas.id_dosen = tb_dosen.id_dosen where tb_kelas.id_dosen = '$idLogin'");
                                        foreach ($data as $t) {
                                            ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
<!--                                                <td>--><?php //= $t['nidn']; ?><!--</td>-->
<!--                                                <td>--><?php //= $t['nama_dosen']; ?><!--</td>-->
                                                <td><?= $t['nama_kelas']; ?></td>
                                                <td><?= $t['matakuliah']; ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-info btn-icon-md"
                                                            onclick="location.href='?page=mahasiswa&&id=<?= $t['id_kelas'] ?>'"><i
                                                                class="fa-solid fa-list"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/tab-content-->
                </div>
            </div>
        </div>
    </div>
</div>