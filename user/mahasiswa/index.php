<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="card dz-card" id="accordion-one">
                    <div class="card-header flex-wrap">
                        <div>
                            <h4 class="card-title">Data Mahasiswa</h4>
                        </div>
                        <ul class="nav nav-tabs dzm-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button type="button" class="btn btn-rounded btn-info" data-bs-target="#tambah"
                                        data-bs-toggle="modal"><span
                                            class="btn-icon-start text-info"><i
                                                class="fa fa-plus color-info"></i></span> Tambah
                                </button>
                            </li>
                        </ul>
                    </div>
                    <!--tab-content-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Preview" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card-body pt-0">
                                <div class="table-responsive">
                                    <table id="tabel" class="display table" style="min-width: 845px">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Foto Mahasiswa</th>
                                            <th>NIM</th>
                                            <th>Nama Mahasiswa</th>
                                            <th>Telpon Mahasiswa</th>
                                            <th>Alamat Mahasiswa</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $kelas = $_GET['id'];
                                        $no = 1;
                                        $data = mysqli_query($koneksi, "SELECT * FROM tb_mahasiswa where id_kelas = '$kelas'");
                                        foreach ($data as $t) {
                                            ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td>
                                                    <img src="uploads/<?= $t['foto_mahasiswa']; ?>" alt="mahasiswa"
                                                         width="200">
                                                </td>
                                                <td><?= $t['nim']; ?></td>
                                                <td><?= $t['nama_mahasiswa']; ?></td>
                                                <td><?= $t['telp_mahasiswa']; ?></td>
                                                <td><?= $t['alamat_mahasiswa']; ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-icon-md"
                                                            data-bs-target="#edit" data-bs-toggle="modal"
                                                            data-id="<?= $t['id_mahasiswa'] ?>"
                                                            data-nim="<?= $t['nim'] ?>"
                                                            data-nama="<?= $t['nama_mahasiswa'] ?>"
                                                            data-alamat="<?= $t['alamat_mahasiswa'] ?>"
                                                            data-telp="<?= $t['telp_mahasiswa'] ?>"
                                                    ><i
                                                                class="fa-solid fa-edit"></i></button>
                                                    <button type="button" class="btn btn-danger btn-icon-md"
                                                            data-bs-target="#hapus" data-bs-toggle="modal"
                                                            data-id="<?= $t['id_mahasiswa'] ?>"
                                                            data-nim="<?= $t['nim'] ?>"
                                                    ><i
                                                                class="fa-solid fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/tab-content-->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fadeIn" id="tambah">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Mahasiswa</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="basic-form">
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">NIM</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIM" required name="nim"
                                       id="nim" maxlength="30">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Mahasiswa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Mahasiswa" required
                                       name="nama"
                                       id="nama" maxlength="100">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Telpon Mahasiswa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Telpon Mahasiswa" required
                                       name="telp"
                                       id="telp" maxlength="15">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Alamat Mahasiswa</label>
                            <div class="col-sm-9">
                                <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Foto Mahasiswa</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" required
                                       name="foto" accept="image/*">
                            </div>
                        </div>
                        <hr>
                        <span class="text-danger">* Usahakan wajah terlihat jelas</span>
                        <hr>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 1</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" required
                                       name="f1" accept="image/*">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 2</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" required
                                       name="f2" accept="image/*">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 3</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" required
                                       name="f3" accept="image/*">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 4</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" required
                                       name="f4" accept="image/*">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 5</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control" required
                                       name="f5" accept="image/*">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fadeIn" id="edit">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Mahasiswa</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="basic-form">
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">NIM</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIM" readonly required name="nim"
                                       id="nim" maxlength="30">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Mahasiswa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Mahasiswa" required
                                       name="nama"
                                       id="nama" maxlength="100">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Telpon Mahasiswa</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Telpon Mahasiswa" required
                                       name="telp"
                                       id="telp" maxlength="15">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Alamat Mahasiswa</label>
                            <div class="col-sm-9">
                                <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Foto Mahasiswa</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control"
                                       name="foto" accept="image/*">
                            </div>
                        </div>
<!--                        <hr>-->
<!--                        <span class="text-danger">* Usahakan wajah terlihat jelas</span>-->
<!--                        <hr>-->
<!--                        <div class="mb-3 row">-->
<!--                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 1</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="file" class="form-control"-->
<!--                                       name="f1" accept="image/*">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="mb-3 row">-->
<!--                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 2</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="file" class="form-control"-->
<!--                                       name="f2" accept="image/*">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="mb-3 row">-->
<!--                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 3</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="file" class="form-control" required-->
<!--                                       name="f3" accept="image/*">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="mb-3 row">-->
<!--                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 4</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="file" class="form-control" required-->
<!--                                       name="f4" accept="image/*">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="mb-3 row">-->
<!--                            <label class="col-sm-3 col-form-label">Foto Validasi Absen Wajah 5</label>-->
<!--                            <div class="col-sm-9">-->
<!--                                <input type="file" class="form-control" required-->
<!--                                       name="f5" accept="image/*">-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="edit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="hapus">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data Mahasiswa</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" id="id" name="id">
                <input type="hidden" id="nim" name="nim">
                <div class="modal-body">
                    <h4>Yakin ingin menghapus data ini ?</h4>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="hapus" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="../vendor/global/global.min.js"></script>

<script>
    $(document).ready(function () {
        $("#edit").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let kelas = button.data('kelas');
            let nama = button.data('nama');
            let nim = button.data('nim');
            let alamat = button.data('alamat');
            let telp = button.data('telp');
            let modal = $('#edit');
            modal.find('#id').val(id);
            modal.find('#nim').val(nim);
            modal.find('#nama').val(nama);
            modal.find('#alamat').val(alamat);
            modal.find('#telp').val(telp);
        });
        $("#hapus").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let nim = button.data('nim');
            let modal = $('#hapus');
            modal.find('#id').val(id);
            modal.find('#nim').val(nim);
        });
    });
</script>
<?php
if (isset($_POST['simpan'])) {
    $nim = htmlspecialchars($_POST['nim']);
    $nama = htmlspecialchars($_POST['nama']);
    $telp = htmlspecialchars($_POST['telp']);
    $alamat = htmlspecialchars($_POST['alamat']);
    $foto = $_FILES['foto'];
    $f1 = $_FILES['f1'];
    $f2 = $_FILES['f2'];
    $f3 = $_FILES['f3'];
    $f4 = $_FILES['f4'];
    $f5 = $_FILES['f5'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM `tb_mahasiswa` WHERE nim = '$nim' AND id_kelas = '$kelas'"));

    if ($cek) {
        echo "<script>
            Swal.fire({
              icon: 'info',
              title: 'Mahasiswa sudah ada!',
              showConfirmButton: true,
            }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
            });
        </script>";
    } else {
        if ($foto['name'] && $f1['name'] && $f2['name']) {
            $ekstensi = pathinfo($foto['name'])['extension'];
            $namaFoto = rand(1000, 9999) . '.' . $ekstensi;

            $ekstensiF1 = pathinfo($foto['name'])['extension'];
            $namaFotoF1 = '1.' . $ekstensi;

            $ekstensiF2 = pathinfo($foto['name'])['extension'];
            $namaFotoF2 = '2.' . $ekstensi;

            $ekstensiF3 = pathinfo($foto['name'])['extension'];
            $namaFotoF3 = '3.' . $ekstensi;

            $ekstensiF4 = pathinfo($foto['name'])['extension'];
            $namaFotoF4 = '4.' . $ekstensi;

            $ekstensiF5 = pathinfo($foto['name'])['extension'];
            $namaFotoF5 = '5.' . $ekstensi;

            if ($ekstensiF1 === 'jpg' && $ekstensiF2 === 'jpg') {

                if (!is_dir("../labels/" . $nim . "/")) {
                    mkdir("../labels/" . $nim . "/");
                }

                move_uploaded_file($f1["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF1);
                move_uploaded_file($f2["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF2);
                move_uploaded_file($f3["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF3);
                move_uploaded_file($f4["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF4);
                move_uploaded_file($f5["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF5);

                $cekUpload = move_uploaded_file($foto['tmp_name'], 'uploads/' . $namaFoto);


                $simpan = mysqli_query($koneksi, "INSERT INTO tb_mahasiswa VALUES (null,'$kelas','$nim','$nama','$alamat','$telp','$namaFoto','$namaFotoF1','$namaFotoF2','$namaFotoF3','$namaFotoF4','$namaFotoF5')");

                if ($simpan) {
                    echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
                } else {
                    echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
                }
            } else {
                echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'Foto validasi harus format JPG!',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
            }
        }
    }
}

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $nim = htmlspecialchars($_POST['nim']);
    $nama = htmlspecialchars($_POST['nama']);
    $telp = htmlspecialchars($_POST['telp']);
    $alamat = htmlspecialchars($_POST['alamat']);
    $foto = $_FILES['foto'];
    $f1 = $_FILES['f1'];
    $f2 = $_FILES['f2'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM `tb_mahasiswa` WHERE nim = '$nim' AND id_kelas = '$kelas' and id_mahasiswa != '$id'"));

    if ($cek) {
        echo "<script>
            Swal.fire({
              icon: 'info',
              title: 'Mahasiswa sudah ada!',
              showConfirmButton: true,
            }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
            });
        </script>";
    } else {
        if ($foto['name']) {
            $ekstensi = pathinfo($foto['name'])['extension'];
            $namaFoto = rand(1000, 9999) . '.' . $ekstensi;
            if ($f1['name'] && $f2['name']) {
                $ekstensiF1 = pathinfo($foto['name'])['extension'];
                $namaFotoF1 = '1.' . $ekstensi;

                $ekstensiF2 = pathinfo($foto['name'])['extension'];
                $namaFotoF2 = '2.' . $ekstensi;

                if ($ekstensiF1 === 'jpg' && $ekstensiF2 === 'jpg') {

                    if (!is_dir("../labels/" . $nim . "/")) {
                        mkdir("../labels/" . $nim . "/");
                    }
                    move_uploaded_file($f1["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF1);
                    move_uploaded_file($f2["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF2);

                } else {
                    echo "<script>
                    Swal.fire({
                      icon: 'info',
                      title: 'Foto validasi harus format JPG!',
                      showConfirmButton: true,
                    }).then((result) => {
                              location='?page=mahasiswa&&id=$kelas';
                    });
                    </script>";
                }
            }
            $cekUpload = move_uploaded_file($foto['tmp_name'], 'uploads/' . $namaFoto);

            $edit = mysqli_query($koneksi, "UPDATE tb_mahasiswa SET nama_mahasiswa = '$nama',
                        id_kelas = '$kelas',
                        alamat_mahasiswa = '$alamat',
                        telp_mahasiswa = '$telp',
                        foto_mahasiswa = '$namaFoto' WHERE id_mahasiswa = '$id'
                        ");

            if ($edit) {
                echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Diubah.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
            } else {
                echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Diubah.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
            }
        } else {
            if ($f1['name'] && $f2['name']) {
                $ekstensiF1 = pathinfo($foto['name'])['extension'];
                $namaFotoF1 = '1.' . $ekstensi;

                $ekstensiF2 = pathinfo($foto['name'])['extension'];
                $namaFotoF2 = '2.' . $ekstensi;

                if ($ekstensiF1 === 'jpg' && $ekstensiF2 === 'jpg') {

                    if (!is_dir("../labels/" . $nim . "/")) {
                        mkdir("../labels/" . $nim . "/");
                    }
                    move_uploaded_file($f1["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF1);
                    move_uploaded_file($f2["tmp_name"], "../labels/" . $nim . "/" . $namaFotoF2);

                } else {
                    echo "<script>
                    Swal.fire({
                      icon: 'info',
                      title: 'Foto validasi harus format JPG!',
                      showConfirmButton: true,
                    }).then((result) => {
                              location='?page=mahasiswa&&id=$kelas';
                    });
                    </script>";
                }

                $edit = mysqli_query($koneksi, "UPDATE tb_mahasiswa SET nama_mahasiswa = '$nama',
                        id_kelas = '$kelas',
                        alamat_mahasiswa = '$alamat',
                        telp_mahasiswa = '$telp' WHERE id_mahasiswa = '$id'
                        ");

                if ($edit) {
                    echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Diubah.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
                } else {
                    echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Diubah.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
                }
            } else {

                $edit = mysqli_query($koneksi, "UPDATE tb_mahasiswa SET nama_mahasiswa = '$nama',
                        id_kelas = '$kelas',
                        alamat_mahasiswa = '$alamat',
                        telp_mahasiswa = '$telp' WHERE id_mahasiswa = '$id'
                        ");

                if ($edit) {
                    echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Diubah.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
                } else {
                    echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Diubah.',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
                }
            }
        }
    }
}

if (isset($_REQUEST['hapus'])) {
    $id = $_REQUEST['id'];
    $nim = $_REQUEST['nim'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_laporan where nim = '" . $nim . "' AND id_kelas = '" . $kelas . "'"));
    if ($cek) {
        echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'Data gagal terhapus karena data terpakai!',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
    } else {
        $hapus = mysqli_query($koneksi, "delete from tb_mahasiswa where id_mahasiswa = '" . $id . "'");
        if ($hapus) {
            echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Dihapus!',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
        } else {
            echo "<script>
    Swal.fire({
      icon: 'warning',
      title: 'Data Gagal Dihapus!',
      showConfirmButton: true,
    }).then((result) => {
              location='?page=mahasiswa&&id=$kelas';
    });
    </script>";
        }
    }
}