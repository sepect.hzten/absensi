<?php
$dosen = $_GET['dosen'];
?>
<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="card dz-card" id="accordion-one">
                    <div class="card-header flex-wrap">
                        <div>
                            <h4 class="card-title">Data Kelas</h4>
                        </div>
                        <ul class="nav nav-tabs dzm-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button type="button" class="btn btn-rounded btn-info" data-bs-target="#tambah"
                                        data-bs-toggle="modal"><span
                                            class="btn-icon-start text-info"><i
                                                class="fa fa-plus color-info"></i></span> Tambah
                                </button>
                            </li>
                        </ul>
                    </div>
                    <!--tab-content-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Preview" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card-body pt-0">
                                <div class="table-responsive">
                                    <table id="tabel" class="display table" style="min-width: 845px">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Kelas</th>
                                            <th>Mata Kuliah</th>
                                            <th>Semester</th>
                                            <th>SKS</th>
                                            <th>Waktu/Jadwal</th>
                                            <th>Ruangan</th>
                                            <th>Hari Mata Kuliah</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        $data = mysqli_query($koneksi, "SELECT * FROM tb_kelas inner join tb_dosen on tb_kelas.id_dosen = tb_dosen.id_dosen where tb_kelas.id_dosen = '$dosen'");
                                        foreach ($data as $t) {
                                            ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $t['nama_kelas']; ?></td>
                                                <td><?= $t['matakuliah']; ?></td>
                                                <td><?= $t['semester']; ?></td>
                                                <td><?= $t['sks']; ?></td>
                                                <td><?= $t['waktu']; ?></td>
                                                <td><?= $t['ruangan']; ?></td>
                                                <td><?= $t['hari']; ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-icon-md"
                                                            data-bs-target="#edit" data-bs-toggle="modal"
                                                            data-id="<?= $t['id_kelas'] ?>"
                                                            data-dosen="<?= $t['id_dosen'] ?>"
                                                            data-nama="<?= $t['nama_kelas'] ?>"
                                                            data-matakuliah="<?= $t['matakuliah'] ?>"
                                                            data-hari="<?= $t['hari'] ?>"
                                                            data-waktu="<?= $t['waktu'] ?>"
                                                            data-sks="<?= $t['sks'] ?>"
                                                            data-ruangan="<?= $t['ruangan'] ?>"
                                                            data-semester="<?= $t['semester'] ?>"
                                                    ><i
                                                                class="fa-solid fa-edit"></i></button>
                                                    <button type="button" class="btn btn-danger btn-icon-md"
                                                            data-bs-target="#hapus" data-bs-toggle="modal"
                                                            data-id="<?= $t['id_kelas'] ?>"><i
                                                                class="fa-solid fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/tab-content-->
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fadeIn" id="tambah">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Kelas</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <form action="" method="post">
                <div class="modal-body">
                    <div class="basic-form">
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Dosen</label>
                            <div class="col-sm-9">
                                <select name="dosen" id="dosen" class="form-control" required>
                                    <option value="" disabled selected>Pilih Dosen</option>
                                    <?php
                                    $data = mysqli_query($koneksi, "SELECT * FROM tb_dosen");
                                    foreach ($data as $t) {
                                        ?>
                                        <option value="<?= $t['id_dosen'] ?>"><?= $t['nama_dosen'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Kelas</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Kelas" required name="nama"
                                       id="nama" maxlength="100">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Mata Kuliah</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Mata Kuliah" required name="matakuliah"
                                       id="matakuliah" maxlength="50">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Hari Mata Kuliah</label>
                            <div class="col-sm-9">
                                <select name="hari" id="hari" class="form-control" required>
                                    <option value="SENIN">SENIN</option>
                                    <option value="SELASA">SELASA</option>
                                    <option value="RABU">RABU</option>
                                    <option value="KAMIS">KAMIS</option>
                                    <option value="JUMAT">JUMAT</option>
                                    <option value="SABTU">SABTU</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Jumlah SKS</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" placeholder="Jumlah SKS" required name="sks"
                                       id="sks" min="1" max="50">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Semester</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Semester" required name="semester"
                                       id="semester" maxlength="50">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Ruangan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Ruangan" required name="ruangan"
                                       id="ruangan" maxlength="50">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fadeIn" id="edit">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Kelas</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <form action="" method="post">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="basic-form">
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Dosen</label>
                            <div class="col-sm-9">
                                <select name="dosen" id="dosen" class="form-control" required>
                                    <option value="" disabled selected>Pilih Dosen</option>
                                    <?php
                                    $data = mysqli_query($koneksi, "SELECT * FROM tb_dosen");
                                    foreach ($data as $t) {
                                        ?>
                                        <option value="<?= $t['id_dosen'] ?>"><?= $t['nama_dosen'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Kelas</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Kelas" required name="nama"
                                       id="nama" maxlength="100">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Mata Kuliah</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Mata Kuliah" required name="matakuliah"
                                       id="matakuliah" maxlength="50">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Hari Mata Kuliah</label>
                            <div class="col-sm-9">
                                <select name="hari" id="hari" class="form-control" required>
                                    <option value="SENIN">SENIN</option>
                                    <option value="SELASA">SELASA</option>
                                    <option value="RABU">RABU</option>
                                    <option value="KAMIS">KAMIS</option>
                                    <option value="JUMAT">JUMAT</option>
                                    <option value="SABTU">SABTU</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Jumlah SKS</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" placeholder="Jumlah SKS" required name="sks"
                                       id="sks" min="1" max="50">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Semester</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Semester" required name="semester"
                                       id="semester" maxlength="50">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Ruangan</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Ruangan" required name="ruangan"
                                       id="ruangan" maxlength="50">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="edit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="hapus">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data Kelas</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" id="id" name="id">
                <div class="modal-body">
                    <h4>Yakin ingin menghapus data ini ?</h4>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="hapus" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="../vendor/global/global.min.js"></script>


<script>
    $(document).ready(function () {
        $("#edit").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let dosen = button.data('dosen');
            let nama = button.data('nama');
            let matakuliah = button.data('matakuliah');
            let waktu = button.data('waktu');
            let sks = button.data('sks');
            let semester = button.data('semester');
            let ruangan = button.data('ruangan');
            let hari = button.data('hari');
            let modal = $('#edit');
            modal.find('#id').val(id);
            modal.find('#dosen').val(dosen);
            modal.find('#nama').val(nama);
            modal.find('#matakuliah').val(matakuliah);
            modal.find('#hari').val(hari);
            modal.find('#ruangan').val(ruangan);
            modal.find('#semester').val(semester);
            modal.find('#sks').val(sks);
            modal.find('#waktu').val(waktu);
        });
        $("#hapus").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let modal = $('#hapus');
            modal.find('#id').val(id);
        });
    });
</script>
<?php
if (isset($_POST['simpan'])) {
    $dosen = $_POST['dosen'];
    $nama = $_POST['nama'];
    $matakuliah = $_POST['matakuliah'];
    $semester = $_POST['semester'];
    $sks = $_POST['sks'];
    $waktu = $_POST['waktu'];
    $hari = $_POST['hari'];
    $ruangan = $_POST['ruangan'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_kelas where nama_kelas = '$nama' and id_dosen = '$dosen'"));

    if ($cek) {
        echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'Kelas sudah ada!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
    } else {

        $simpan = mysqli_query($koneksi, "INSERT INTO tb_kelas VALUES (null,'$dosen','$nama','$matakuliah','$hari','$waktu','$sks,'$ruangan,'$semester')");

        if ($simpan) {
            echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
        } else {
            echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
        }
    }
}

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $dosen = $_POST['dosen'];
    $matakuliah = $_POST['matakuliah'];
    $semester = $_POST['semester'];
    $sks = $_POST['sks'];
    $waktu = $_POST['waktu'];
    $hari = $_POST['hari'];
    $ruangan = $_POST['ruangan'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_kelas where nama_kelas = '$nama' and id_dosen = '$dosen' and id_kelas != '$id'"));

    if ($cek) {
        echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'Kelas sudah ada!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
    } else {

        $edit = mysqli_query($koneksi, "UPDATE tb_kelas SET nama_kelas = '$nama', id_dosen = '$dosen'
                  ,matakuliah = '$matakuliah'
                  ,sks = '$sks'
                  ,semester = '$semester'
                  ,ruangan = '$ruangan'
                  ,waktu = '$waktu'
                  ,hari = '$hari'
                WHERE id_kelas = '$id'");

        if ($edit) {
            echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Diubah.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
        } else {
            echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Diubah.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
        }
    }
}

if (isset($_REQUEST['hapus'])) {
    $id = $_REQUEST['id'];
    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_laporan where id_kelas = '$id'"));
    if ($cek) {
        echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data gagal terhapus karena data terpakai!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
    } else {
        $hapus = mysqli_query($koneksi, "delete from tb_kelas where id_kelas = '" . $id . "'");
        if ($hapus) {
            echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Dihapus!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
        } else {
            echo "<script>
    Swal.fire({
      icon: 'warning',
      title: 'Data Gagal Dihapus!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=kelas&&dosen=$dosen';
    });
    </script>";
        }
    }
}