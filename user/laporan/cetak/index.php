<?php
$kelas = $_GET['kelas'];

require ('../../../fpdf/fpdf.php');

include '../../koneksi.php';

$dt = mysqli_fetch_assoc(mysqli_query($koneksi,"SELECT * FROM tb_kelas where id_kelas = '$kelas'"));

$data = mysqli_query($koneksi, "SELECT A.id_laporan as idLaporan,A.id_kelas as idKelas,C.nama_kelas as namaKelas, A.nim as nimL, D.nama_mahasiswa as namaMahasiswa FROM tb_laporan A INNER JOIN tb_dosen B ON A.id_dosen = B.id_dosen INNER JOIN tb_kelas C ON A.id_kelas = C.id_kelas INNER JOIN tb_mahasiswa D ON A.nim = D.nim where A.id_kelas = '$kelas' GROUP BY A.tanggal_absen, A.id_kelas, A.nim");


$pdf = new FPDF('P', 'mm', 'A4');
$pdf->AddPage();

$pdf->SetFont('Times', 'B', 13);
$pdf->Cell(200, 10, 'LAPORAN ABSENSI KELAS ' . strtoupper($dt['nama_kelas']), 0, 0, 'C');

$pdf->Cell(10, 15, '', 0, 1);
$pdf->SetFont('Times', 'B', 9);
$pdf->Cell(10, 7, 'NO', 1, 0, 'C');
$pdf->Cell(50, 7, 'NAMA', 1, 0, 'C');
$pdf->Cell(23, 7, 'STATUS', 1, 0, 'C');
$pdf->Cell(20, 7, 'HONOR', 1, 0, 'C');
$pdf->Cell(23, 7, 'KONSUMSI', 1, 0, 'C');
$pdf->Cell(20, 7, 'TOTAL', 1, 0, 'C');
$pdf->Cell(40, 7, 'TTD', 1, 0, 'C');

$pdf->Output('D', 'Laporan.pdf');