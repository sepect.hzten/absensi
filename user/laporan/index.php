<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <?php
            if (isset($_POST['cari'])) {
                $kelas = $_POST['kelas'];
                $dt = mysqli_fetch_assoc(mysqli_query($koneksi, "SELECT * FROM tb_kelas WHERE id_kelas = '$kelas'"));
                ?>
                <div class="col-xl-12">
                    <div class="card dz-card" id="accordion-one">
                        <div class="card-header flex-wrap">
                            <div class="col-6">
                                <h4 class="card-title">Laporan Absensi</h4>
                                <hr>
                                <form action="" method="post">
                                    <div class="col-12">
                                        <div class="mb-3 row">
                                            <label class="col-sm-3 col-form-label">Kelas</label>
                                            <div class="col-sm-9">
                                                <select name="kelas" id="kelas" class="form-control" required>
                                                    <option value="" disabled selected>Pilih Kelas</option>
                                                    <?php
                                                    if ($_SESSION['jenis'] === 'admin'){
                                                        $datas = mysqli_query($koneksi, "SELECT * FROM tb_kelas");
                                                        foreach ($datas as $t) {
                                                            ?>
                                                            <option value="<?= $t['id_kelas'] ?>" <?= $t['id_kelas'] === $kelas ? 'selected' : '' ?>><?= $t['nama_kelas'] ?></option>
                                                            <?php
                                                        }
                                                    }elseif ($_SESSION['jenis'] === 'dosen'){
                                                        $datas = mysqli_query($koneksi, "SELECT * FROM tb_kelas WHERE id_dosen = '$idLogin'");
                                                        foreach ($datas as $t) {
                                                            ?>
                                                            <option value="<?= $t['id_kelas'] ?>" <?= $t['id_kelas'] === $kelas ? 'selected' : '' ?>><?= $t['nama_kelas'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <div class="col-sm-4">
                                                <button type="submit" name="cari" class="btn btn-primary">Tampilkan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--tab-content-->
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="Preview" role="tabpanel"
                                 aria-labelledby="home-tab">
                                <div class="card-body pt-0">
                                    <a href="../cetak/cetak.php?kelas=<?= $kelas ?>" target="_blank"><button class="btn btn-info">Cetak Laporan <i class="fa fa-print"></i></button></a>
                                    <hr>
                                    <div class="table-responsive">
                                        <table id="tabel" class="display table" style="min-width: 845px">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NIM</th>
                                                <th>Nama Mahasiswa</th>
                                                <th>Kelas</th>
                                                <th>Total Hadir</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $no = 1;
                                            $data = mysqli_query($koneksi, "SELECT A.id_laporan as idLaporan,A.id_kelas as idKelas,C.nama_kelas as namaKelas, A.nim as nimL, D.nama_mahasiswa as namaMahasiswa FROM tb_laporan A INNER JOIN tb_dosen B ON A.id_dosen = B.id_dosen INNER JOIN tb_kelas C ON A.id_kelas = C.id_kelas INNER JOIN tb_mahasiswa D ON A.nim = D.nim where A.id_kelas = '$kelas' GROUP BY namaKelas, nimL");
                                            foreach ($data as $t) {
                                                $totalHadir = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_laporan WHERE nim = '" . $t['nimL'] . "' AND id_kelas = '" . $t['idKelas'] . "'"));
                                                ?>
                                                <tr>
                                                    <td><?= $no++; ?></td>
                                                    <td><?= $t['nimL']; ?></td>
                                                    <td><?= $t['namaMahasiswa']; ?></td>
                                                    <td><?= $t['namaKelas']; ?></td>
                                                    <td><?= $totalHadir; ?></td>
                                                    <td>
                                                        <?php
                                                        if ($_SESSION['jenis'] === 'dosen'){
                                                            ?>
                                                            <button type="button" class="btn btn-info btn-icon-md"
                                                                    onclick="location.href='?page=detailmahasiswa&&id=<?= $t['idLaporan'] ?>'">
                                                                <i
                                                                        class="fa-solid fa-list"></i></button>
                                                        <?php
                                                        }else{
                                                        ?>
                                                        <button type="button" class="btn btn-info btn-icon-md"
                                                                onclick="location.href='?page=detailmhs&&id=<?= $t['idLaporan'] ?>'">
                                                            <i
                                                                    class="fa-solid fa-list"></i></button>
                                                        <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/tab-content-->
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="col-xl-12">
                    <div class="card dz-card" id="accordion-one">
                        <div class="card-header flex-wrap">
                            <div class="col-6">
                                <h4 class="card-title">Laporan Absensi</h4>
                                <hr>
                                <form action="" method="post">
                                    <div class="col-12">
                                        <div class="mb-3 row">
                                            <label class="col-sm-3 col-form-label">Kelas</label>
                                            <div class="col-sm-9">
                                                <select name="kelas" id="kelas" class="form-control" required>
                                                    <option value="" disabled selected>Pilih Kelas</option>
                                                    <?php
                                                    if ($_SESSION['jenis'] === 'admin'){
                                                        $datas = mysqli_query($koneksi, "SELECT * FROM tb_kelas");
                                                        foreach ($datas as $t) {
                                                            ?>
                                                            <option value="<?= $t['id_kelas'] ?>" <?= $t['id_kelas'] === $kelas ? 'selected' : '' ?>><?= $t['nama_kelas'] ?></option>
                                                            <?php
                                                        }
                                                    }elseif ($_SESSION['jenis'] === 'dosen'){
                                                        $datas = mysqli_query($koneksi, "SELECT * FROM tb_kelas WHERE id_dosen = '$idLogin'");
                                                        foreach ($datas as $t) {
                                                            ?>
                                                            <option value="<?= $t['id_kelas'] ?>" <?= $t['id_kelas'] === $kelas ? 'selected' : '' ?>><?= $t['nama_kelas'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="mb-3 row">
                                            <div class="col-sm-4">
                                                <button type="submit" name="cari" class="btn btn-primary">Tampilkan
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!--tab-content-->
                        <!--/tab-content-->
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>