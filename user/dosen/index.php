<div class="content-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="card dz-card" id="accordion-one">
                    <div class="card-header flex-wrap">
                        <div>
                            <h4 class="card-title">Data Dosen</h4>
                        </div>
                        <ul class="nav nav-tabs dzm-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button type="button" class="btn btn-rounded btn-info" data-bs-target="#tambah"
                                        data-bs-toggle="modal"><span
                                            class="btn-icon-start text-info"><i
                                                class="fa fa-plus color-info"></i></span> Tambah
                                </button>
                            </li>
                        </ul>
                    </div>
                    <!--tab-content-->
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="Preview" role="tabpanel" aria-labelledby="home-tab">
                            <div class="card-body pt-0">
                                <div class="table-responsive">
                                    <table id="tabel" class="display table" style="min-width: 845px">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIDN</th>
                                            <th>Nama Dosen</th>
                                            <th>Telpon Dosen</th>
                                            <th>Alamat Dosen</th>
                                            <th>Aksi</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $no = 1;
                                        $data = mysqli_query($koneksi, "SELECT * FROM tb_dosen");
                                        foreach ($data as $t) {
                                            ?>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td><?= $t['nidn']; ?></td>
                                                <td><?= $t['nama_dosen']; ?></td>
                                                <td><?= $t['telp_dosen']; ?></td>
                                                <td><?= $t['alamat_dosen']; ?></td>
                                                <td>
                                                    <button type="button" class="btn btn-success btn-icon-md"
                                                            data-bs-target="#edit" data-bs-toggle="modal"
                                                            data-id="<?= $t['id_dosen'] ?>"
                                                            data-nidn="<?= $t['nidn'] ?>"
                                                            data-nama="<?= $t['nama_dosen'] ?>"
                                                            data-alamat="<?= $t['alamat_dosen'] ?>"
                                                            data-telp="<?= $t['telp_dosen'] ?>"
                                                            data-username="<?= $t['username'] ?>"
                                                            data-password="<?= $t['password'] ?>"
                                                    ><i
                                                                class="fa-solid fa-edit"></i></button>
                                                    <button type="button" class="btn btn-danger btn-icon-md"
                                                            data-bs-target="#hapus" data-bs-toggle="modal"
                                                            data-id="<?= $t['id_dosen'] ?>"><i
                                                                class="fa-solid fa-trash"></i></button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/tab-content-->
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fadeIn" id="tambah">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Dosen</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <form action="" method="post">
                <div class="modal-body">
                    <div class="basic-form">
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">NIDN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIDN" required name="nidn"
                                       id="nidn" maxlength="30">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Dosen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Dosen" required name="nama"
                                       id="nama" maxlength="100">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Telpon Dosen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Telpon Dosen" required name="telp"
                                       id="telp" maxlength="15">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Alamat Dosen</label>
                            <div class="col-sm-9">
                                <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Username Dosen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Username Dosen" required
                                       name="username"
                                       id="username" maxlength="15">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Password Dosen</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" placeholder="Password Dosen" required
                                       name="password"
                                       id="password" maxlength="15">
                                <hr>
                                <div class="form-check custom-checkbox mb-3">
                                    <input type="checkbox" class="form-check-input" id="cekPass" onchange="onKlik(this)">
                                    <label class="form-check-label" for="cekPass">Lihat Password</label>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fadeIn" id="edit">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Data Dosen</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal">
                </button>
            </div>
            <form action="" method="post">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <div class="basic-form">
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">NIDN</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="NIDN" required name="nidn"
                                       id="nidn" maxlength="30">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Nama Dosen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Nama Dosen" required name="nama"
                                       id="nama" maxlength="100">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Telpon Dosen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Telpon Dosen" required name="telp"
                                       id="telp" maxlength="15">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Alamat Dosen</label>
                            <div class="col-sm-9">
                                <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control"
                                          required></textarea>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Username Dosen</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" placeholder="Username Dosen" required
                                       name="username"
                                       id="username" maxlength="15">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label class="col-sm-3 col-form-label">Password Dosen</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" placeholder="Password Dosen" required
                                       name="password"
                                       id="password2" maxlength="15">
                                <hr>
                                <div class="form-check custom-checkbox mb-3">
                                    <input type="checkbox" class="form-check-input" id="cekPass" onchange="onKlikP(this)">
                                    <label class="form-check-label" for="cekPass">Lihat Password</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="edit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="hapus">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hapus Data Dosen</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
            </div>
            <form action="" method="post">
                <input type="hidden" id="id" name="id">
                <div class="modal-body">
                    <h4>Yakin ingin menghapus data ini ?</h4>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger light" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" name="hapus" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="../vendor/global/global.min.js"></script>

<script>
    function onKlik(el) {
        $('#password').attr('type',el.checked ? 'text' : 'password');
    }
    function onKlikP(el) {
        $('#password2').attr('type',el.checked ? 'text' : 'password');
    }
</script>

<script>
    $(document).ready(function () {
        $("#edit").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let nama = button.data('nama');
            let nidn = button.data('nidn');
            let alamat = button.data('alamat');
            let telp = button.data('telp');
            let username = button.data('username');
            let password = button.data('password');
            let modal = $('#edit');
            modal.find('#id').val(id);
            modal.find('#nidn').val(nidn);
            modal.find('#nama').val(nama);
            modal.find('#alamat').val(alamat);
            modal.find('#telp').val(telp);
            modal.find('#username').val(username);
            modal.find('#password2').val(password);
        });
        $("#hapus").on("show.bs.modal", function (event) {
            let button = $(event.relatedTarget);
            let id = button.data('id');
            let modal = $('#hapus');
            modal.find('#id').val(id);
        });
    });
</script>
<?php
if (isset($_POST['simpan'])) {
    $nidn = $_POST['nidn'];
    $nama = $_POST['nama'];
    $telp = $_POST['telp'];
    $alamat = $_POST['alamat'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_dosen where nidn = '$nidn'"));
    $cek2 = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_dosen where username = '$username'"));


    $cekPass = strlen($password);

    if ($cekPass >= 8) {
        echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'Password minimal 8 karakter!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
    } else {

        if ($cek) {
            echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'NIDN sudah ada!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        } elseif ($cek2) {
            echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Username sudah ada!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        }

        $simpan = mysqli_query($koneksi, "INSERT INTO tb_dosen VALUES (null,'$nidn','$nama','$alamat','$telp','$username','$password')");

        if ($simpan) {
            echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        } else {
            echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Disimpan.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        }
    }
}

if (isset($_POST['edit'])) {
    $id = $_POST['id'];
    $nidn = $_POST['nidn'];
    $nama = $_POST['nama'];
    $telp = $_POST['telp'];
    $alamat = $_POST['alamat'];
    $username = $_POST['username'];
    $password = $_POST['password'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_dosen where nidn = '$nidn' and id_dosen != '$id'"));
    $cek2 = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_dosen where username = '$username' and id_dosen != '$id'"));


    $cekPass = strlen($password);

    if ($cekPass >= 8) {
        echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'Password minimal 8 karakter!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
    } else {

        if ($cek) {
            echo "<script>
    Swal.fire({
      icon: 'info',
      title: 'NIDN sudah ada!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        } elseif ($cek2) {
            echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Username sudah ada!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        }

        $edit = mysqli_query($koneksi, "UPDATE tb_dosen SET 
                    username = '$username',
                    nama_dosen = '$nama',
                    nidn = '$nidn',
                    alamat_dosen = '$alamat',
                    telp_dosen = '$telp',
                    password = '$password' WHERE id_dosen = '$id'");

        if ($edit) {
            echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Diubah.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        } else {
            echo "<script>
    Swal.fire({
      icon: 'error',
      title: 'Data Gagal Diubah.',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        }
    }
}

if (isset($_REQUEST['hapus'])) {
    $id = $_REQUEST['id'];

    $cek = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM tb_laporan where id_dosen = '" . $id . "'"));
    if ($cek) {
        echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data gagal terhapus karena data terpakai!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
    } else {
        $hapus = mysqli_query($koneksi, "delete from tb_dosen where id_dosen = '" . $id . "'");
        if ($hapus) {
            echo "<script>
    Swal.fire({
      icon: 'success',
      title: 'Data Berhasil Dihapus!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        } else {
            echo "<script>
    Swal.fire({
      icon: 'warning',
      title: 'Data Gagal Dihapus!',
      showConfirmButton: true,
    }).then((result) => {
      location='?page=dosen';
    });
    </script>";
        }
    }
}