<?php
//mulai session
ob_start();
session_start();

include 'koneksi.php';
//cek session
if (isset($_SESSION['status'])) {
    header("Location: ./user/index.php");
    die();
}

if (isset($_REQUEST['login'])) {
    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];

    $sql = mysqli_query($koneksi, "select * from tb_admin where username = '" . $username . "' and password = '" . $password . "'");

    $sql2 = mysqli_query($koneksi, "select * from tb_dosen where username = '" . $username . "' and password = '" . $password . "'");

    $cek_login = mysqli_num_rows($sql);
    $cek_login2 = mysqli_num_rows($sql2);

    if ($cek_login > 0) {
        $data = mysqli_fetch_assoc($sql);
        $_SESSION['id'] = $data['id_admin'];
        $_SESSION['nama'] = $data['nama_admin'];
        $_SESSION['status'] = "login";
        $_SESSION['jenis'] = "admin";
        header("Location: ./user/index.php");
        die();
    } elseif ($cek_login2 > 0) {
        $data = mysqli_fetch_assoc($sql2);
        $_SESSION['id'] = $data['id_dosen'];
        $_SESSION['nama'] = $data['nama_dosen'];
        $_SESSION['status'] = "login";
        $_SESSION['jenis'] = "dosen";
        header("Location: ./user/index.php");
        die();
    } else {
        echo '
        <script>alert("Login gagal");
        history.back();
        </script>';
    }
}


?>
<!DOCTYPE html>
<html lang="en" class="h-100">


<!-- Mirrored from yashadmin.dexignzone.com/xhtml/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Jan 2024 15:41:02 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <meta name="robots" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Yashadmin:Sales Management System Admin Bootstrap 5 Template">
    <meta property="og:title" content="Yashadmin:Sales Management System Admin Bootstrap 5 Template">
    <meta property="og:description" content="Yashadmin:Sales Management System Admin Bootstrap 5 Template">
    <meta property="og:image" content="page-error-404.html">
    <meta name="format-detection" content="telephone=no">

    <!-- PAGE TITLE HERE -->
    <title>Login</title>

    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/png" href="images/favicon.png">
    <link href="vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body class="vh-100">
<div class="page-wraper">
    <!-- Content -->
    <div class="browse-job login-style3">
        <!-- Coming Soon -->
        <div class="bg-img-fix overflow-hidden" style="background:#fff url(images/background/bg6.jpg); height: 100vh;">
            <div class="row gx-0">
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-12 vh-100 bg-white ">
                    <div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside"
                         style="max-height: 653px;" tabindex="0">
                        <div class="mCSB_container" style="position:relative; top:0; left:0;" dir="ltr">
                            <div class="login-form style-2">
                                <div class="card-body">
                                    <nav>
                                        <div class="nav nav-tabs border-bottom-0" id="nav-tab" role="tablist">
                                            <div class="tab-content w-100" id="nav-tabContent">
                                                <div class="tab-pane fade show active" id="nav-personal" role="tabpanel"
                                                     aria-labelledby="nav-personal-tab">
                                                    <form action="" class="dz-form pb-3" method="post">
                                                        <h3 class="form-title m-t0">Silahkan Login</h3>
                                                        <div class="dz-separator-outer m-b5">
                                                            <div class="dz-separator bg-primary style-liner"></div>
                                                        </div>
                                                        <p>Masukkan Username dan Password Anda. </p>
                                                        <div class="form-group mb-3">
                                                            <input type="text" name="username" autocomplete="off"
                                                                   placeholder="Username" required class="form-control"
                                                                   maxlength="30">
                                                        </div>
                                                        <div class="form-group mb-3">
                                                            <input type="password" name="password" id="password"
                                                                   placeholder="Password" class="form-control" required
                                                                   maxlength="20">
                                                            <hr>
                                                            <div class="form-check custom-checkbox mb-3">
                                                                <input type="checkbox" class="form-check-input" id="cekPass" onchange="onKlik(this)">
                                                                <label class="form-check-label" for="cekPass">Lihat Password</label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group text-left forget-main">
                                                            <button type="submit" name="login" class="btn btn-primary">
                                                                Login
                                                            </button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Full Blog Page Contant -->
    </div>
    <!-- Content END-->
</div>

<!--**********************************
	Scripts
***********************************-->
<!-- Required vendors -->
<script src="vendor/global/global.min.js"></script>
<script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="js/deznav-init.js"></script>
<script src="js/custom.js"></script>
<script src="js/demo.js"></script>

<script>
    function onKlik(el) {
        $('#password').attr('type',el.checked ? 'text' : 'password');
    }
</script>
</body>

<!-- Mirrored from yashadmin.dexignzone.com/xhtml/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 21 Jan 2024 15:41:02 GMT -->
</html>